package de.unipassau.fim.ep.jobjogger.gateway.config;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedClientException;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

/**
 * Own Zuul PreFilter to modify the Authorization Header for the Requests to the Dashboard which it
 * reads from the secret after the authentication has been done. Also sets the Cookie from a query
 * parameter and reads token from cookie if it exists
 */
public class PreFilter extends ZuulFilter {

    @Value("${microservice-secret.secret-token}")
    private String token;

    @Value("${secure.cookie}")
    private boolean secureCookie;

    private final String cookieName = "token";

    private RestOperations restTemplate = new RestTemplate();

    @Value("${host.auth-server}")
    private String authServerHost;

    /**
     * Classifies filter type as pre filter
     *
     * @return String representing that type
     */
    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    /**
     * Defines the filter order
     *
     * @return int order of a filter
     */
    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * A bool value to determine if run() method should be invoked
     *
     * @return bool value
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * Core method of a zuul filter, which exchanges the Auth Header with the Service Acc token,
     * validates Auth token saved in a cookie and also sets cookie from query param if the cookie
     * doesnt exist
     *
     * @return nothing since not necessary
     * @throws ZuulException if problem occurs
     */
    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        HttpServletResponse response = ctx.getResponse();

        // checks if cookie exists
        if (getToken(request) != null) {
            HttpHeaders headers = new HttpHeaders();
            Map<String, Object> map = executePost("http://"+authServerHost+"/auth/oauth/check_token?token=" + getToken(request), headers);
            if (map == null || map.isEmpty() || map.get("error") != null) {
                // if cookie exists with invalid token and request params contains new set it
                if (getQueryParams(request) != null) {
                    setCookie(request, response);
                    return null;
                }
                throw new InvalidTokenException("Token in Cookie expired");
            }
            ArrayList<String> list = new ArrayList<>(
                (Collection<? extends String>) map.get("authorities"));
            // removes query parameter when cookie is still valid
            if (getQueryParams(request) != null) {
                setCookie(request, response);
                return null;
            }
            // validates token
            if (map.get("active").equals(true) && list.contains("ROLE_ADMIN")) {
                ctx.addZuulRequestHeader("Authorization", "Bearer " + token);
            } else {
                throw new UnauthorizedClientException("No valid token or Role");
            }
          // if no cookie exists checks if token request param exists and sets cookie
        } else if (getQueryParams(request) != null) {
            setCookie(request, response);
        } else {
            throw new UnauthorizedClientException("no token or cookie");
        }
        return null;
    }

    private String getToken(HttpServletRequest request) {
        if (request.getCookies() != null) {
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    private String getQueryParams(HttpServletRequest request) {
        if (request.getQueryString() != null && request.getQueryString().contains("token=")) {
            return request.getQueryString().replaceFirst("token=", "");
        }
        return null;
    }

    private void setCookie(HttpServletRequest request, HttpServletResponse response) {
        try {
            Cookie cookie = new Cookie(cookieName, getQueryParams(request));
            cookie.setHttpOnly(true);
            cookie.setSecure(secureCookie);
            response.addCookie(cookie);
            response.sendRedirect(request.getRequestURI());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Map<String, Object> executePost(String path, HttpHeaders headers) {
        try {
            if (headers.getContentType() == null) {
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            }
            @SuppressWarnings("rawtypes")
            Map map = restTemplate.exchange(path, HttpMethod.POST, new HttpEntity<MultiValueMap<String, String>>(null, headers), Map.class).getBody();
            @SuppressWarnings("unchecked")
            Map<String, Object> result = map;
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
