# JobJogger Gateway
This repository contains the code of our JobJogger Gateway. 
It's a Zuul Proxy for securing the Kubernetes Dashboard for admins.

## Environment Variables
| Name                     | Description                          |
|--------------------------|--------------------------------------|
| AUTH_SERVER_HOST         | Hostname of the Authorization Server |
| DASHBOARD_USER           | Dashboard User                       |
| SECURE_COOKIE            | Should Cookie be secured?            |

