package de.unipassau.fim.ep.jobjogger.gateway.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Specify resource-server specific properties and configure the access rules
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    /**
     * Configures that all endpoints are permitted
     *
     * @param http the http filter
     * @throws Exception when theres a problem
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                    .antMatchers("/**").permitAll();

        http.cors().and().csrf().disable();
    }

}
